#!/usr/bin/env bash
set -e

CMARK_DIR=./cmark/src
BUILD_DIR=../../scripts

(cd $CMARK_DIR && $BUILD_DIR/command_line.sh)

