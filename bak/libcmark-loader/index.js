module.exports = function rawLoader(source) {
    const json = JSON.stringify(source)
        .replace('WebAssembly.instantiateStreaming(fetch(wasmBinaryFile,{credentials:"same-origin"}),info)', '=========');

    // console.log('======================');
    // console.log(json);
        // .replace(/\u2028/g, '\\u2028')
        // .replace(/\u2029/g, '\\u2029');
    return `module.exports = ${json}`;

    // return json;
};