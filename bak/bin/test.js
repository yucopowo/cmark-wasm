const fs = require("fs");
const path = require("path");

let result = fs.readFileSync(path.resolve(__dirname, "libcmark.wasm"));

// console.log(result);

const array = [];

for (const b of result) {
    array.push(b);
}

// console.log( JSON.stringify(array) );

// fs.writeFile

fs.writeFile('./bin.js',  'module.exports = \n'+JSON.stringify(array)+';' , {  }, function(err) {
    if (err) {
        throw err;
    }
    console.log('success');
});