const cmark = require('../../index');
const md = require('./test.md');
const unified = require('unified');
const processor = unified()
    .use(require('rehype-parse'), {fragment:true});
import { SaxEventType, SAXParser } from './sax-wasm';




async function parse(text, options = {}) {
    console.time('cmark');
    const html = await cmark(text, options);
    console.timeEnd('cmark');
    return html;
}

function render(html) {
    console.time('render');
    const app = document.getElementById('app');
    app.innerHTML = html;
    console.timeEnd('render');
}

async function init() {
    await cmark.load();
}

async function task() {
    console.time('task');

    const html = await parse(md, {
        footnotes: true,
        unsafe: true,
        // width: 100
        sourcepos: true,
        hardbreaks: true
    });
    
    processor.parse(html);
    console.time('html parse');
    const hast = processor.parse(html);
    console.timeEnd('html parse');
    // console.log(hast);

    render(html);

    console.timeEnd('task');

    return html;
}

function sleep() {
    return new Promise(function (resolve) {
        setTimeout(function () {
            resolve();
        }, 1000);
    })
}


async function loadAndPrepareWasm() {
    const saxWasmResponse = await fetch('./sax-wasm.wasm');
    const saxWasmbuffer = await saxWasmResponse.arrayBuffer();
    // const parser = new SAXParser(SaxEventType.Attribute | SaxEventType.OpenTag);
    const parser = new SAXParser(SaxEventType.Text | SaxEventType.OpenTag | SaxEventType.CloseTag);
    // parser.events = SaxEventType.Text | SaxEventType.OpenTag | SaxEventType.Attribute | SaxEventType.CloseTag;

    // Instantiate and prepare the wasm for parsing
    const ready = await parser.prepareWasm(new Uint8Array(saxWasmbuffer));
    if (ready) {
        return parser;
    }
}


function processDocument(parser, html) {

    return new Promise(function (resolve) {

        const list = [];

        parser.eventHandler = (event, data) => {

            // if (event === SaxEventType.Attribute ) {
            //     // process attribute
            // } else {
            //     // process open tag
            //
            //     console.log(data);
            //
            // }

            // console.log('event', event);
            if (event === SaxEventType.Text ) {
                list.push(data);
                // console.log('Text', data);
            }
            else if (event === SaxEventType.OpenTag ) {
                list.push(data);
                // console.log('OpenTag', data);
            }
            else if (event === SaxEventType.Attribute ) {

                // console.log('Attribute', data);

            }
            else if (event === SaxEventType.CloseTag ) {

                // console.log('CloseTag', data);
                list.push(data);

                if(data.name === "root") {
                    resolve(list);
                }

            }








        };


        parser.write(html);
        parser.end();

    });


}


(async ()=>{

    await init();

    // const text = '# h1';

    // const html = parse(md);

    const html = await task();
    // await sleep();
    //
    // await task();
    // await sleep();
    //
    // await task();

    // console.log(html);
    const text = '<root>'+html+'</root>';
    // console.log(text);

    console.time('load parser');

    loadAndPrepareWasm().then(async function (parser) {
        console.timeEnd('load parser');

        await processDocument(parser, text);
        console.time('sax parse');
        const list = await processDocument(parser, text);
        console.timeEnd('sax parse');

        console.log(list);

    });


})();
































