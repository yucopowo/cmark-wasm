#!/usr/bin/env bash
set -e

basepath=$(cd `dirname $0`; pwd)

cd $basepath/../node_modules/cmark-gfm

pwd

if [ ! -d build  ];then
  mkdir build
else
  echo build exist
fi

cd build

cmake ..

make

make test