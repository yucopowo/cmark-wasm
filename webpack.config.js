const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const production = (process.env.NODE_ENV === 'production');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: production?'production':'none',
    entry: {
        'cmark': './src/index.js'
    },
    module: {
        rules: [

            { test: /\.md$/, use: 'text-loader' },
            {
                test: /libcmark.js$/,
                use: [
                    {
                        loader:'webpack-replace-loader',
                        options: {
                            arr: [


                                {
                                    search: 'libcmark.wasm',
                                    replace: 'cmark.wasm',
                                    attr: 'g'
                                },

                                // {
                                //     search: 'Module = Module || {};',
                                //     replace: 'Module = Module || {};\n'+BIN_DATA(),
                                //     attr: 'g'
                                // },
                                // {
                                //     search: 'WebAssembly.instantiateStreaming(fetch(wasmBinaryFile,{credentials:"same-origin"}),info)',
                                //     replace: 'WebAssembly.instantiate(BIN_DATA, info)',
                                //     attr: 'g'
                                // },
                                {
                                    search: 'if(returnType==="string")return Pointer_stringify(ret);',
                                    replace: 'if(returnType==="string"){var foo=Pointer_stringify(ret);if(ident==="cmark_markdown_to_html")_free(ret);return foo}',
                                    attr: 'g'
                                },

                            ]
                        }
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.js'],
        alias: {
            'libcmark': path.resolve(__dirname, 'lib/libcmark')
        }
    },
    output: {
        filename: production?'[name].min.js':'[name].js',
        path: path.resolve(__dirname, 'dist'),

        library: 'cmark',
        libraryTarget: 'umd',
    },
    node: {
        fs: "empty"
    },
    plugins: [
        new CopyWebpackPlugin([{
            from: 'lib/libcmark.wasm', to: './cmark.wasm', toType: 'file'
        }]),
        new HtmlWebpackPlugin({
            filename: 'demo.html',
            template: 'examples/demo.html',
            inject: false
        })
    ],
};