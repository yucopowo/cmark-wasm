const path = require('path');
const production = (process.env.NODE_ENV === 'production');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const fs = require('fs');
const VirtualModulesPlugin = require("webpack-virtual-modules");

module.exports = {
    mode: production?'production':'none',
    entry: {
        'cmark': './src/index.js'
    },
    module: {
        rules: [

            { test: /\.md$/, use: 'text-loader' },
            {
                test: /libcmark.js$/,
                use: [
                    {
                        loader:'webpack-replace-loader',
                        options: {
                            arr: [


                                // {
                                //     search: 'libcmark.wasm',
                                //     replace: 'cmark.wasm',
                                //     attr: 'g'
                                // },
                                // {
                                //     search: 'Module = Module || {};',
                                //     replace: 'Module = Module || {};\n'+BIN_DATA(),
                                //     attr: 'g'
                                // },
                                // {
                                //     search: 'WebAssembly.instantiateStreaming(fetch(wasmBinaryFile,{credentials:"same-origin"}),info)',
                                //     replace: 'WebAssembly.instantiate(BIN_DATA, info)',
                                //     attr: 'g'
                                // },
                                // {
                                //     search: 'if(returnType==="string")return Pointer_stringify(ret);',
                                //     replace: 'if(returnType==="string"){var foo=Pointer_stringify(ret);if(ident==="cmark_markdown_to_html")_free(ret);return foo}',
                                //     attr: 'g'
                                // },


                                {
                                    search: 'Module = Module || {};',
                                    replace: 'Module = Module || {};\nconst BIN_DATA = require("module-wasm");\nModule.BIN_DATA = BIN_DATA;',
                                    attr: 'g'
                                },
                                {
                                    search: 'if(returnType==="string")return Pointer_stringify(ret);',
                                    replace: 'if(returnType==="string"){var foo=Pointer_stringify(ret);if(ident==="cmark_markdown_to_html")_free(ret);return foo}',
                                    attr: 'g'
                                },
                                {
                                    search: 'WebAssembly.instantiateStreaming(fetch(wasmBinaryFile,{credentials:"same-origin"}),info)',
                                    replace: 'WebAssembly.instantiate(Module.BIN_DATA, info)',
                                    attr: 'g'
                                }



                            ]
                        }
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.js'],
        alias: {
            'libcmark': path.resolve(__dirname, 'lib/libcmark')
        }
    },
    output: {
        filename: production?'[name].all.min.js':'[name].all.js',
        path: path.resolve(__dirname, 'dist'),

        library: 'cmark',
        libraryTarget: 'umd',
    },
    node: {
        fs: "empty"
    },
    plugins: [
        new VirtualModulesPlugin({
            'node_modules/module-wasm.js': (function () {
                const buf = fs.readFileSync(path.resolve(__dirname, './lib/', "libcmark.wasm"));
                const Zlib = require('./bin/rawdeflate.min').Zlib;
                const deflate = new Zlib.RawDeflate(buf);
                const compressed = deflate.compress();
                return 'module.exports =[\n'+compressed.toString()+'];\n';
            })()
        }),
        // new CopyWebpackPlugin([{
        //     from: 'lib/libcmark.wasm', to: './cmark.wasm', toType: 'file'
        // }]),
        new HtmlWebpackPlugin({
            filename: 'demo.html',
            template: 'examples/demo.html',
            inject: false
        })
    ],
};