const { bytesLength } = require('./util');
const createOptions = require('./options');

const Module = {
    parser: null
};

const LIB = require('libcmark');

function load() {
    return new Promise(async function (resolve) {
        LIB().then(function (M) {
            Module.parser = M.cwrap('cmark_markdown_to_html', 'string', ['string', 'number', 'number']);
            resolve();
        });
    });
}

async function cmark(text, options = {}) {
    if(!Module.parser) {
        await load();
    }
    return Module.parser(text, bytesLength(text), createOptions(options))
}

cmark.load = load;

module.exports = cmark;