const CMARK_OPT_DEFAULT =0;
const CMARK_OPT_SOURCEPOS =(1 << 1);
const CMARK_OPT_HARDBREAKS =(1 << 2);
const CMARK_OPT_SAFE =(1 << 3);
const CMARK_OPT_NOBREAKS =(1 << 4);
const CMARK_OPT_NORMALIZE =(1 << 8);
const CMARK_OPT_VALIDATE_UTF8 =(1 << 9);
const CMARK_OPT_SMART =(1 << 10);



const CMARK_OPT_GITHUB_PRE_LANG =(1 << 11);
const CMARK_OPT_LIBERAL_HTML_TAG =(1 << 12);
const CMARK_OPT_FOOTNOTES =(1 << 13);
const CMARK_OPT_STRIKETHROUGH_DOUBLE_TILDE =(1 << 14);
const CMARK_OPT_TABLE_PREFER_STYLE_ATTRIBUTES =(1 << 15);
const CMARK_OPT_FULL_INFO_STRING =(1 << 16);
const CMARK_OPT_UNSAFE =(1 << 17);

module.exports = function createOptions(config) {
    let options = CMARK_OPT_DEFAULT;

    if (config.safe) {
        options |= CMARK_OPT_SAFE;
    }

    if (config.unsafe) {
        options |= CMARK_OPT_UNSAFE;
    }

    if (config.sourcepos) {
        options |= CMARK_OPT_SOURCEPOS;
    }

    if (config.hardbreaks) {
        options |= CMARK_OPT_HARDBREAKS;
    }

    if (config.footnotes) {
        options |= CMARK_OPT_FOOTNOTES;
    }

    return options;
};
