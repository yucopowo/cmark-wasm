const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const fs = require("fs");
const VirtualModulesPlugin = require("webpack-virtual-modules");

module.exports = {
    mode: 'none',
    entry: {
        main: path.resolve(__dirname, './src/index.js')
    },
    module: {
        rules: [
            { test: /\.md$/, use: 'text-loader' },
            {
                test: /libcmark.js$/,
                use: [
                    {
                        loader:'webpack-replace-loader',
                        options: {
                            arr: [
                                {
                                    search: 'Module = Module || {};',
                                    replace: 'Module = Module || {};\nconst BIN_DATA = require("module-wasm");\nModule.BIN_DATA = BIN_DATA;',
                                    attr: 'g'
                                },
                                {
                                    search: 'if(returnType==="string")return Pointer_stringify(ret);',
                                    replace: 'if(returnType==="string"){var foo=Pointer_stringify(ret);if(ident==="cmark_markdown_to_html")_free(ret);return foo}',
                                    attr: 'g'
                                },
                                {
                                    search: 'WebAssembly.instantiateStreaming(fetch(wasmBinaryFile,{credentials:"same-origin"}),info)',
                                    replace: 'WebAssembly.instantiate(Module.BIN_DATA, info)',
                                    attr: 'g'
                                }
                            ]
                        }
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.js'],
        alias: {
            'libcmark': path.resolve(__dirname, '../lib/libcmark')
        }
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'www')
    },
    node: {
        fs: "empty"
    },
    devServer: {
        inline: false,
        hot: false,
        contentBase: path.join(__dirname, "../lib")
    },
    plugins: [
        new VirtualModulesPlugin({
            'node_modules/module-wasm.js': (function () {
                const result = fs.readFileSync(path.resolve(__dirname, '../lib/', "libcmark.wasm"));
                const array = [];
                for (const b of result) {
                    array.push(b);
                }
                return 'module.exports = new Uint8Array(\n'+JSON.stringify(array, null, 2)+')\n;'
            })()
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'examples/index.html'
        })
    ],
};