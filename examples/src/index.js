const cmark = require('../../index');
const md = require('./test.md');
// const unified = require('unified');
// const processor = unified()
//     .use(require('rehype-parse'), {fragment:true});

async function parse(text, options = {}) {
    console.time('cmark');
    const html = await cmark(text, options);
    console.timeEnd('cmark');
    return html;
}

function render(html) {
    console.time('render');
    const app = document.getElementById('app');
    app.innerHTML = html;
    console.timeEnd('render');
}

async function init() {
    await cmark.load();
}

async function task() {
    console.log('=====================');
    console.time('all');

    const html = await parse(md, {
        footnotes: true,
        unsafe: true,
        // width: 100
        sourcepos: true,
        hardbreaks: true
    });

    // console.time('html parse');
    // const hast = processor.parse(html);
    // console.timeEnd('html parse');
    // console.log(hast);

    render(html);

    console.timeEnd('all');

    return html;
}

function sleep(time) {
    return new Promise(function (resolve) {
        setTimeout(function () {
            resolve();
        }, time || 1000);
    })
}

(async ()=>{

    await init();
    await task();

    // await task();
    // await sleep(3000);
    // await task();

})();
































