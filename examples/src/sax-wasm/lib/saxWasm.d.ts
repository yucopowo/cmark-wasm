export declare class SaxEventType {
    static Text: number;
    static ProcessingInstruction: number;
    static SGMLDeclaration: number;
    static Doctype: number;
    static Comment: number;
    static OpenTagStart: number;
    static Attribute: number;
    static OpenTag: number;
    static CloseTag: number;
    static OpenCDATA: number;
    static Cdata: number;
    static CloseCDATA: number;
}
export interface Position {
    line: number;
    character: number;
}
export interface Attribute {
    name: string;
    value: string;
    nameStart: Position;
    nameEnd: Position;
    valueStart: Position;
    valueEnd: Position;
}
export interface Text {
    value: string;
    start: Position;
    end: Position;
}
export interface Tag {
    name: string;
    attributes: Attribute[];
    textNodes: Text[];
    selfClosing: boolean;
    openStart: Position;
    openEnd: Position;
    closeStart: Position;
    closeEnd: Position;
}
export declare class SAXParser {
    static textDecoder: TextDecoder;
    events: number;
    eventHandler: (type: SaxEventType, detail: Tag | Attribute | Position | Text | string) => void;
    private wasmSaxParser;
    constructor(events?: number);
    write(value: string): void;
    end(): void;
    prepareWasm(saxWasm: Uint8Array): Promise<boolean>;
    protected eventTrap: (event: number, ptr: number, len: number) => void;
}
