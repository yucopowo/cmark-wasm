# cmark-wasm
WebAssembly markdown parser with cmark

## install

npm install

## dev

npm run dev

## build

npm run build

## dist

npm run dist